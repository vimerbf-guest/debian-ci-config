package 'rabbitmq-server'
service 'rabbitmq-server'

[
  "/etc/rabbitmq/ca.crt",
  "/etc/rabbitmq/#{node["fqdn"]}.crt",
  "/etc/rabbitmq/#{node["fqdn"]}.key",
].each do |f|
  remote_file f do
    sensitive true
    source    "files/host-#{node['fqdn']}/#{File.basename(f)}"
    owner     'root'
    group     'rabbitmq'
    mode      '0440'
    notifies  :restart, 'service[rabbitmq-server]'
  end
end

file '/etc/rabbitmq/rabbitmq-env.conf' do
  content "RABBITMQ_NODE_IP_ADDRESS=0.0.0.0\n"
  owner       'root'
  group       'rabbitmq'
  mode        '0640'
  notifies :restart, 'service[rabbitmq-server]'
end

file '/etc/rabbitmq/rabbitmq.config' do
  action :delete
end

template '/etc/rabbitmq/rabbitmq.conf' do
  owner       'root'
  group       'rabbitmq'
  mode        '0640'
  notifies :restart, 'service[rabbitmq-server]'
end

execute 'rabbitmq-plugins enable rabbitmq_management' do
  user        'root'
  not_if      "rabbitmq-plugins list | grep -q '^.E.*rabbitmq_management'"
end
