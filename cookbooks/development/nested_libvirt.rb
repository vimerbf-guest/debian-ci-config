package 'libvirt-clients'
package 'libvirt-daemon-system'
service 'libvirtd'
execute 'change default libvirt net on development VMs' do
  only_if '(ip address show dev eth0 | grep -q 192.168.122) && !(virsh net-list | grep -q default)'
  command 'sed -i -e "s/192.168.122/192.168.123/g" /etc/libvirt/qemu/networks/default.xml'
  notifies :restart, 'service[libvirtd]', :immediately
end
