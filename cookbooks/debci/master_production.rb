execute 'systemctl stop debci-batch.timer && systemctl disable debci-batch.timer' do
  only_if 'systemctl list-timers | grep debci-batch.timer'
end
file '/etc/systemd/system/debci-batch.service' do
  action :delete
  notifies :run, 'execute[systemctl daemon-reload]'
end
file '/etc/systemd/system/debci-batch.timer' do
  action :delete
  notifies :run, 'execute[systemctl daemon-reload]'
end

Array(node['debci']['batch_architectures']).each do |arch|
  template "/etc/systemd/system/debci-batch-#{arch}.timer" do
    source "templates/debci-batch.timer.erb"
    variables arch: arch
    notifies :run, 'execute[systemctl daemon-reload]'
  end
  template "/etc/systemd/system/debci-batch-#{arch}.service" do
    source "templates/debci-batch.service.erb"
    variables arch: arch
    notifies :run, 'execute[systemctl daemon-reload]'
  end
end

execute 'systemctl daemon-reload' do
  action :nothing
  notifies :run, 'execute[enable-batch]'
end

enable_batch_command = Array(node['debci']['batch_architectures']).map do |arch|
  "systemctl enable debci-batch-#{arch}.timer && systemctl start debci-batch-#{arch}.timer"
end.join(' && ')
execute 'enable-batch' do
  command enable_batch_command
  action :nothing
end unless enable_batch_command.empty?

# SSL validation token
file '/etc/debci/head.html' do
  action :delete
end
# file '/etc/debci/head.html' do
#   content '<meta name="globalsign-domain-verification" content="q7fyEFC_zuumpoWGfO8XmAc_uwYS6eD3RLoxBu-WgH" />' + "\n"
#   notifies :run, 'execute[debci-generate-html]'
# end
# execute 'debci-generate-html' do
#   user 'debci'
#   command 'debci generate-html'
#   action :nothing
# end
