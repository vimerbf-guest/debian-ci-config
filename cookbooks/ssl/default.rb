package 'ssl-cert'

site = node['web_hostname']

if site == 'debci-master.local'
  directory '/var/lib/dehydrated'
  directory '/var/lib/dehydrated/certs'
  directory '/var/lib/dehydrated/certs/debci-master.local'
  link '/var/lib/dehydrated/certs/debci-master.local/cert.pem' do
    to      '/etc/ssl/certs/ssl-cert-snakeoil.pem'
    force true
  end
  link '/var/lib/dehydrated/certs/debci-master.local/fullchain.pem' do
    to      '/etc/ssl/certs/ssl-cert-snakeoil.pem'
    force true
  end
  link '/var/lib/dehydrated/certs/debci-master.local/privkey.pem' do
    to '/etc/ssl/private/ssl-cert-snakeoil.key'
    force true
  end
else
  package 'dehydrated'
  package 'dehydrated-apache2'

  file "/etc/dehydrated/domains.txt" do
    owner     'root'
    group     'root'
    mode      '0640'
    content   "#{site}\n"
    notifies  :run, 'execute[dehydrated]'
  end

  remote_file '/etc/systemd/system/dehydrated.service' do
    owner     'root'
    group     'root'
    mode      '0640'
    notifies  :run, 'execute[install dehydrated service]'
  end

  remote_file '/etc/systemd/system/dehydrated.timer' do
    owner     'root'
    group     'root'
    mode      '0640'
    notifies  :run, 'execute[install dehydrated timer]'
  end

  execute 'install dehydrated service' do
    command   'systemctl daemon-reload'
    action    :nothing
  end

  execute 'install dehydrated timer' do
    command   'systemctl daemon-reload && systemctl enable dehydrated.timer && systemctl restart dehydrated.timer'
    action    :nothing
  end

  execute 'dehydrated' do
    command   'systemctl start dehydrated.service'
    action    :nothing
  end

end
