namespace :psql do
  desc 'Gets the amount of pending jobs per arch/suite'
  task :pending do
    Rake::Task['run:' + $master_node.hostname].invoke('cd /tmp ; sudo -u debci psql -c "select arch,suite,count(*) from jobs where status is null group by suite,arch ;"')
  end

  desc 'Gets the amount of pending jobs per suite/trigger'
  task :triggers do
    Rake::Task['run:' + $master_node.hostname].invoke('cd /tmp ; sudo -u debci psql -c "select suite,split_part(trigger, \' \', 1) as trig,count(*) as cnt from jobs where status is null group by suite,trig order by cnt;"')
  end

  desc 'Gets the amount of pending jobs older than 5 days per arch/suite'
  task :old do
    Rake::Task['run:' + $master_node.hostname].invoke('cd /tmp ; sudo -u debci psql -c "select arch,suite,count(*) from jobs where status is null and created_at < now() - interval \'5 days\' group by suite,arch ;"')
  end

  desc 'tmpfails pending jobs older than 5 days'
  task :tmpfail do
    Rake::Task['run:' + $master_node.hostname].invoke('cd /tmp ; sudo -u debci psql -c "update jobs set date = now(), status = \'tmpfail\', duration_seconds = 0, version = \'n/a\', message = \'elbrus\' where status is null and created_at < now() - interval \'5 days\' ;"')
  end
  
end
